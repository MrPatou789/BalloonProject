class Balloon {
    constructor() {
        this._x = getRandomInt(0, 100)
        this._y = 100
        this._color = getRandomColor()
        this._speed = getRandomInt(0.8, 1)
        this._xMouvement = 0
        this._xAmplitude = 0.3
        this._div = null
    }

    get x() {
        return this._x
    }
    get y() {
        return this._y
    }
    get color() {
        return this._color
    }
    get speed() {
        return this._speed
    }
    get xMouvement() {
        return this._xMouvement
    }
    get xAmplitude() {
        return this._xAmplitude
    }
    get div() {
        return this._div
    }

    set x(x) {
        this._x = x
    }
    set y(y) {
        this._y = y
    }
    set color(color) {
        this._color = color
    }
    set speed(speed) {
        this._speed = speed
    }
    set xMouvement(xMouvement) {
        this._xMouvement = xMouvement
    }
    set xAmplitude(xAmplitude) {
        this._xAmplitude = xAmplitude
    }
    set div(div) {
        this._div = div
    }

    effect(){}

}

class BalloonClock extends Balloon {
    constructor() {
        super()
        this._backgroundImage = 'clock.svg'
        this._color = 'transparent'
    }

    get backgroundImage() {
        return this._backgroundImage
    }

    effect(){
        M.balloons.forEach(balloon => {
            balloon.speed = 0
        })
        setTimeout(function () {
            M.balloons.forEach(balloon => {
                balloon.speed = getRandomInt(0.8, 1)
            })
        }, 2000)
    }
}

class BalloonBomb extends Balloon {
    constructor() {
        super()
        this._backgroundImage = 'bomb.svg'
        this._color = 'transparent'
    }
    get backgroundImage() {
        return this._backgroundImage
    }
    
    effect(){
        M.balloons.forEach(balloon => {
            if (balloon.y < this.y) {
                C.removeBalloon(balloon)
            }
        })
    }
}

class BalloonDouble extends Balloon {
    constructor() {
        super()
        this._backgroundImage = 'double-click.svg'
        this._color = 'transparent'
    }
    get backgroundImage() {
        return this._backgroundImage
    }
}

class BalloonTrap extends Balloon {
    constructor() {
        super()
    }
    effect(){
        let nb = getRandomInt(3, 6)
        for (let index = 0; index < nb; index++) {
            M.addBalloon()
        }
    }
}



function getRandomInt(min, max) {
    return Math.floor(Math.random() * ((max + 1) - min)) + min;
}

function getRandomColor() {
    return color = {
        r: getRandomInt(0, 255),
        g: getRandomInt(0, 255),
        b: getRandomInt(0, 255)
    }
}

function randomX(balloon) {
    let xMouvement = balloon.xMouvement
    let xPosition = balloon.x

    let random = (Math.cos(xMouvement += 0.05) * balloon.xAmplitude)
    balloon.div.style.left = xPosition + random + "%"
    balloon.xMouvement = xMouvement
    balloon.x = xPosition + random

}

document.getElementById('app').addEventListener('click', function (e) {
    let xMouse = e.clientX * 100 / window.innerWidth
    let yMouse = e.clientY * 100 / window.innerHeight
    M.balloons.forEach(balloon => {
        if (xMouse >= balloon.x && xMouse <= balloon.x + 5 && yMouse >= balloon.y && yMouse <= balloon.y + 5) {
            balloon.effect()
            C.removeBalloon(balloon)
        }
    });
})


var M = {
    balloons: [],
    MAX_BALLOON: 50,
    addBalloon: function () {
        let rand = getRandomInt(0, 100)
        let b
        if (rand < 5) {
            b = new BalloonTrap()
        } else if (rand < 10) {
            b = new BalloonDouble()
        } else if (rand < 15) {
            b = new BalloonClock
        } else if (rand < 20) {
            b = new BalloonBomb
        } else {
            b = new Balloon()
        }
        M.balloons.push(b)
        V.render(b)
        requestAnimationFrame(function () {
            V.step(b)
        });
    },
    removeBalloon(balloon) {
        M.balloons = M.balloons.filter(element => element !== balloon)
    }
}

var C = {
    init: function () {
        M.addBalloon()
    },

    removeBalloon(balloon) {
        V.addClassClicked(balloon)
        setTimeout(function () {
            V.removeBalloon(balloon)
            M.removeBalloon(balloon)
        }, 2000)
    },

}

var V = {
    render: function (balloon) {
        const app = document.getElementById('app')
        const div = document.createElement('div')
        div.style.left = balloon.x + '%'
        div.style.top = balloon.y + '%'
        if (balloon instanceof BalloonBomb || balloon instanceof BalloonClock || balloon instanceof BalloonDouble) {
            div.style.backgroundImage = 'url(assets/' + balloon.backgroundImage + ')'
            div.style.backgroundColor = balloon.color
            div.style.backgroundSize = '50px 50px'
        } else {
            div.style.backgroundColor = 'rgb(' + balloon.color.r + ', ' + balloon.color.g + ', ' + balloon.color.b + ')'
        }
        balloon.div = div
        app.appendChild(div)
    },

    step: function (balloon) {
        let yPosition = balloon.y - balloon.speed
        balloon.div.style.top = yPosition + '%'
        balloon.y = yPosition

        if (balloon.y <= -10) {
            V.backToBottom(balloon)
        }

        if (balloon.speed != 0) {
            randomX(balloon)
        }

        if (M.balloons.length < M.MAX_BALLOON && getRandomInt(0, 100) < 3) {
            M.addBalloon()
        }

        requestAnimationFrame(function () {
            V.step(balloon)
        });
    },

    backToBottom: function (balloon) {
        let randomX = getRandomInt(0, 100)

        balloon.y = 100
        balloon.x = randomX
        balloon.speed = getRandomInt(0.8, 1)
        balloon.xAmplitude = 0.3

        balloon.div.style.top = '100%'
        balloon.div.style.left = randomX + '%'
    },

    removeBalloon(balloon) {
        balloon.div.remove()
    },

    addClassClicked(balloon) {
        balloon.div.classList.add('clicked')
    }
}

C.init()